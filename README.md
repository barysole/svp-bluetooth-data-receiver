## MAIN GOAL AND APPLICATION DESCRIPTION

The main goal of the project is to develop a mobile application for receiving data via Bluetooth
technology and their further processing on devices with the Android operating system. Data that will
be sent via Bluetooth represents coordinates for graph that need to be displayed after on an android
device.

## SERVER APPLICATION
To work with the application you must run the following bluetooth server on your computer - https://gitlab.fel.cvut.cz/barysole/bluetooth-dataserver
Server generates random data that is represented by several pairs of values with float type and sends
it to any recipient that has an established connection. There is a strongly-defined protocol that describes how data will be sent. First of all, the application
sends the float number that represents the number of float pairs which will be sent on the output
stream. After that the application sends a group of random float numbers.

## TEST THE APPLICATION WITHOUT THE SERVER
To test the application without the server you may open some of already prepared data from "test_data" folder.