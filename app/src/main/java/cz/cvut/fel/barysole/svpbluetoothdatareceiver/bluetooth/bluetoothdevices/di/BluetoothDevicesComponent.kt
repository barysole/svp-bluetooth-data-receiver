package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.di

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.di.BluetoothModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.FragmentModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.LocationModule
import dagger.Subcomponent

@Subcomponent(modules = [BluetoothDevicesModule::class, BluetoothModule::class, LocationModule::class, FragmentModule::class])
interface BluetoothDevicesComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(fragmentModule: FragmentModule): BluetoothDevicesComponent
    }

    fun inject(fragment: BluetoothDevicesFragment)
}