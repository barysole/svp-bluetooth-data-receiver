package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums

enum class GraphDataStatus {
    DATA_IS_EMPTY,
    DATA_IS_RECEIVING,
    DATA_IS_RECEIVED,
    ERROR_DURING_RECEIVING_PROCESS,
    DATA_SUCCESEFULLY_SAVED,
    DATA_SUCCESEFULLY_OPENED
}