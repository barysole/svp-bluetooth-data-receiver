package cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils

import android.bluetooth.BluetoothClass
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.DataSourceType

fun parseBtMajorDeviceType(majorDeviceType: Int): DataSourceType {
    return when (majorDeviceType) {
        BluetoothClass.Device.Major.COMPUTER -> {
            DataSourceType.COMPUTER
        }
        BluetoothClass.Device.Major.AUDIO_VIDEO -> {
            DataSourceType.AUDIO_VIDEO
        }
        BluetoothClass.Device.Major.PHONE -> {
            DataSourceType.PHONE
        }
        else -> {
            DataSourceType.UNDEFINED
        }
    }
}