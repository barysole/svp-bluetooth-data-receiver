package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.AppError
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.UserError
import javax.inject.Inject

class FileSystemInteractionDelegate @Inject constructor(
    private val delegateHolder: Fragment
) {

    var onShowErrorListener: ((error: UserError) -> Unit)? = null
    var onFileCreateListener: ((fileUri: Uri) -> Unit)? = null
    var onFileOpenListener: ((fileUri: Uri) -> Unit)? = null

    fun createTextFile(fileName: String) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            //explicit MIME data type
            type = "text/plain"
            putExtra(Intent.EXTRA_TITLE, "$fileName.txt")
        }
        delegateHolder.startActivityForResult(intent, CREATE_TXT_FILE)
    }

    fun openTextFile() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            //explicit MIME data type
            type = "text/plain"
        }
        delegateHolder.startActivityForResult(intent, OPEN_TXT_FILE)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CREATE_TXT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                data?.data?.apply {
                    onFileCreateListener?.invoke(this)
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                onShowErrorListener?.invoke(
                    AppError(delegateHolder.getString(R.string.bt_create_file_error))
                )
            }
        } else if (requestCode == OPEN_TXT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                data?.data?.apply {
                    onFileOpenListener?.invoke(this)
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                onShowErrorListener?.invoke(
                    AppError(delegateHolder.getString(R.string.bt_open_file_error))
                )
            }
        }
    }

    fun onDestroy() {
        onShowErrorListener = null
        onFileCreateListener = null
        onFileOpenListener = null
    }

    companion object {
        private const val CREATE_TXT_FILE = 2000
        private const val OPEN_TXT_FILE = 2001
    }
}