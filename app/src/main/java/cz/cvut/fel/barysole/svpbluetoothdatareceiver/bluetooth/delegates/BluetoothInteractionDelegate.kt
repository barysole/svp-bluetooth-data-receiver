package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_FINISHED
import android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_STARTED
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.*
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.enums.BluetoothActionEnum
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.AppError
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.UserError
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*
import javax.inject.Inject


//todo: split some logic to retain fragment (?)
class BluetoothInteractionDelegate @Inject constructor(
    private val bluetoothAdapter: BluetoothAdapter,
    private val locationManager: LocationManager,
    private var currentFragment: Fragment,
    private var activity: AppCompatActivity,
    private val mainThreadHandler: Handler
) : BtPermissionsDelegate(currentFragment) {

    private lateinit var btReceiver: BroadcastReceiver
    private val pausedActions: MutableSet<BluetoothActionEnum> = mutableSetOf()

    //we assume that app can perform only one connection at the same time
    private var currentConnection: BluetoothSocket? = null
    private var backgroundConnectionThread: Thread? = null

    var onShowErrorListener: ((error: UserError) -> Unit)? = null
    var onDiscoveringStateChangeListener: ((isDiscovering: Boolean) -> Unit)? = null
    var onPairedDeviceRetrievedListener: ((pairedDevices: Set<BluetoothDevice>) -> Unit)? = null
    var onNewDiscoveredDeviceListener: ((newDevice: BluetoothDevice) -> Unit)? = null
    var onDeviceBondFinishedListener: ((bondedDevice: BluetoothDevice) -> Unit)? = null
    var onDeviceBondingStartListener: ((bondingDevice: BluetoothDevice) -> Unit)? = null

    //null if error while connection establishing happened
    var onConnectionResultListener: ((connectionIO: Pair<InputStream, OutputStream>?) -> Unit)? =
        null

    override fun onCreate(savedInstanceState: Bundle?) {
        listener = ::onPermissionRequestFinished
        registerReceiver()
        super.onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val bundle = Bundle()
        for (action: BluetoothActionEnum in pausedActions) {
            when (action) {
                BluetoothActionEnum.START_DISCOVERY -> bundle.putBoolean(
                    BluetoothActionEnum.START_DISCOVERY.toString(),
                    true
                )
                BluetoothActionEnum.STOP_DISCOVERY -> bundle.putBoolean(
                    BluetoothActionEnum.STOP_DISCOVERY.toString(),
                    true
                )
                BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES -> bundle.putBoolean(
                    BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES.toString(),
                    true
                )
                BluetoothActionEnum.GET_CONNECTION_TO_BT_DEVICE -> {}
            }
        }
        //todo: implement process result from detached fragment (now we just refuse current connection)
        if (backgroundConnectionThread?.isAlive == true) {
            bundle.putBoolean(
                BluetoothActionEnum.GET_CONNECTION_TO_BT_DEVICE.toString(),
                true
            )
        }
        outState.putBundle(BT_INTERACTION_DELEGATE_BUNDLE, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity.unregisterReceiver(btReceiver)
        onShowErrorListener = null
        onDiscoveringStateChangeListener = null
        onPairedDeviceRetrievedListener = null
        onNewDiscoveredDeviceListener = null
        onDeviceBondFinishedListener = null
        onDeviceBondingStartListener = null

        //null if error while connection establishing happened
        onConnectionResultListener = null
        currentConnection?.close()
    }

    fun checkPausedAction(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState?.getBundle(BT_INTERACTION_DELEGATE_BUNDLE) ?: return
        if (bundle.getBoolean(BluetoothActionEnum.START_DISCOVERY.toString())) {
            pausedActions.add(BluetoothActionEnum.START_DISCOVERY)
        }
        if (bundle.getBoolean(BluetoothActionEnum.STOP_DISCOVERY.toString())) {
            pausedActions.add(BluetoothActionEnum.STOP_DISCOVERY)
        }
        if (bundle.getBoolean(BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES.toString())) {
            pausedActions.add(BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES)
        }
        if (bundle.getBoolean(BluetoothActionEnum.GET_CONNECTION_TO_BT_DEVICE.toString())) {
            onShowErrorListener?.invoke(
                AppError(activity.getString(R.string.bt_rotating_during_connection_error))
            )
            onConnectionResultListener?.invoke(null)
        }
    }

    //we always check permissions inside checkPermission function
    fun startDeviceDiscovery() {
        if (checkPermissions() && checkBluetoothAndLocationAvailability()) {
            try {
                if (!bluetoothAdapter.isDiscovering) {
                    if (!bluetoothAdapter.startDiscovery()) {
                        //todo: create separate classes for bt errors
                        onShowErrorListener?.invoke(
                            AppError(activity.getString(R.string.bt_start_discovery_error))
                        )
                    }
                }
            } catch (e: SecurityException) {
                showBluetoothPermissionError()
            }
        } else {
            pausedActions.add(BluetoothActionEnum.START_DISCOVERY)
        }
    }

    fun stopDeviceDiscovery() {
        if (checkPermissions() && checkBluetoothAndLocationAvailability()) {
            try {
                if (bluetoothAdapter.isDiscovering) {
                    bluetoothAdapter.cancelDiscovery()
                }
            } catch (e: SecurityException) {
                showBluetoothPermissionError()
            }
        } else {
            pausedActions.add(BluetoothActionEnum.STOP_DISCOVERY)
        }
    }

    fun pairWithDevice(bluetoothDevice: BluetoothDevice) {
        if (checkPermissions() && checkBluetoothAvailability()) {
            try {
                if (bluetoothDevice.createBond()) {
                    onDeviceBondingStartListener?.invoke(bluetoothDevice)
                }
            } catch (e: SecurityException) {
                showBluetoothPermissionError()
            }
        }
    }

    //assume that we already have permissions
    fun retrievePairedDeviceList() {
        if (checkPermissions() && checkBluetoothAvailability()) {
            try {
                onPairedDeviceRetrievedListener?.invoke(bluetoothAdapter.bondedDevices)
            } catch (e: SecurityException) {
                showBluetoothPermissionError()
            }
        } else {
            pausedActions.add(BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES)
        }
    }

    //cancel discovery and drop previous connection when start
    fun getConnectionToDeviceService(deviceMacAddress: String, deviceServiceUUID: UUID) {
        if (checkPermissions() && checkBluetoothAvailability()) {
            try {
                bluetoothAdapter.cancelDiscovery()
                currentConnection?.close()
                val pairedDevices = bluetoothAdapter.bondedDevices
                var requestedDevice: BluetoothDevice? = null
                for (device in pairedDevices) {
                    if (device.address == deviceMacAddress) {
                        requestedDevice = device
                        break
                    }
                }
                val socket =
                    requestedDevice?.createRfcommSocketToServiceRecord(deviceServiceUUID)
                if (socket != null) {
                    //todo: use RxJava instead?
                    backgroundConnectionThread = Thread {
                        try {
                            socket.connect()
                            currentConnection = socket
                            mainThreadHandler.post {
                                onConnectionResultListener?.invoke(
                                    Pair(
                                        socket.inputStream,
                                        socket.outputStream
                                    )
                                )
                            }
                        } catch (e: IOException) {
                            mainThreadHandler.post { onConnectionResultListener?.invoke(null) }
                            showConnectionError(
                                requestedDevice?.name ?: deviceMacAddress, false
                            )
                        }
                    }
                    backgroundConnectionThread!!.start()
                } else {
                    showConnectionError(requestedDevice?.name ?: deviceMacAddress, true)
                }
            } catch (e: SecurityException) {
                mainThreadHandler.post { onConnectionResultListener?.invoke(null) }
                showBluetoothPermissionError()
            }
        } else {
            showConnectionError(deviceMacAddress, true)
        }
    }

    private fun registerReceiver() {
        btReceiver = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    ACTION_FOUND -> {
                        // Discovery has found a device
                        val device: BluetoothDevice? =
                            intent.getParcelableExtra(EXTRA_DEVICE)
                        device?.apply {
                            onNewDiscoveredDeviceListener?.invoke(this)
                        }
                    }
                    ACTION_DISCOVERY_STARTED -> {
                        onDiscoveringStateChangeListener?.invoke(true)
                    }
                    ACTION_DISCOVERY_FINISHED -> {
                        onDiscoveringStateChangeListener?.invoke(false)
                    }
                    ACTION_BOND_STATE_CHANGED -> {
                        val newBondState = intent.getIntExtra(EXTRA_BOND_STATE, -1)
                        val previousBondState = intent.getIntExtra(EXTRA_PREVIOUS_BOND_STATE, -1)
                        val bondedDevice = intent.getParcelableExtra<BluetoothDevice>(EXTRA_DEVICE)
                        if (previousBondState == BOND_BONDING && newBondState == BOND_BONDED) {
                            bondedDevice?.apply {
                                onDeviceBondFinishedListener?.invoke(this)
                            }
                            retrievePairedDeviceList()
                        } else if (previousBondState == BOND_BONDING && newBondState == BOND_NONE) {
                            bondedDevice?.apply {
                                onDeviceBondFinishedListener?.invoke(this)
                            }
                            onShowErrorListener?.invoke(
                                AppError(activity.getString(R.string.bt_pairing_error))
                            )
                        }
                    }
                }
            }
        }

        // Register for broadcasts when a device is discovered.
        val filter = IntentFilter(ACTION_FOUND)
        filter.addAction(ACTION_DISCOVERY_STARTED)
        filter.addAction(ACTION_DISCOVERY_FINISHED)
        filter.addAction(ACTION_BOND_STATE_CHANGED)
        activity.registerReceiver(btReceiver, filter)
    }

    //returns true if bluetooth is available and false if not
    private fun checkBluetoothAndLocationAvailability(): Boolean {
        if (!bluetoothAdapter.isEnabled) {
            requestBluetoothEnable()
            return false
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            requestGpsEnable()
            return false
        }
        return true
    }

    private fun checkBluetoothAvailability(): Boolean {
        if (!bluetoothAdapter.isEnabled) {
            requestBluetoothEnable()
            return false
        }
        return true
    }

    private fun showConnectionError(deviceName: String, isFromMainThread: Boolean) {
        if (isFromMainThread) {
            onShowErrorListener?.invoke(
                AppError(activity.getString(R.string.bt_service_connection_error, deviceName))
            )
            mainThreadHandler.post { onConnectionResultListener?.invoke(null) }
        } else {
            mainThreadHandler.post {
                onShowErrorListener?.invoke(
                    AppError(activity.getString(R.string.bt_service_connection_error, deviceName))
                )
            }
            mainThreadHandler.post { onConnectionResultListener?.invoke(null) }
        }
    }

    //bluetooth is required in manifest
    private fun requestBluetoothEnable() {
        try {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            currentFragment.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        } catch (e: SecurityException) {
            showBluetoothPermissionError()
        }
    }

    private fun requestGpsEnable() {
        val enableGPSIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        currentFragment.startActivityForResult(enableGPSIntent, REQUEST_ENABLE_GPS)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                restartActions()
            } else if (resultCode == RESULT_CANCELED) {
                onShowErrorListener?.invoke(AppError(activity.getString(R.string.bt_is_disabled_error)))
            }
        } else if (requestCode == REQUEST_ENABLE_GPS) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                restartActions()
            } else if (resultCode == RESULT_CANCELED) {
                onShowErrorListener?.invoke(AppError(activity.getString(R.string.bt_is_disabled_error)))
            }
        }
    }

    private fun onPermissionRequestFinished(granted: Boolean) {
        if (granted) {
            restartActions()
        } else {
            showBluetoothPermissionError()
        }
    }

    private fun restartActions() {
        for (action: BluetoothActionEnum in pausedActions) {
            when (action) {
                BluetoothActionEnum.START_DISCOVERY -> startDeviceDiscovery()
                BluetoothActionEnum.STOP_DISCOVERY -> stopDeviceDiscovery()
                BluetoothActionEnum.RETRIEVE_PAIRED_DEVICES -> retrievePairedDeviceList()
                BluetoothActionEnum.GET_CONNECTION_TO_BT_DEVICE -> {}
            }
        }
    }

    private fun showBluetoothPermissionError() {
        //if user removed permission
        onShowErrorListener?.invoke(
            AppError(activity.getString(R.string.bt_permission_error))
        )
    }

    companion object {
        //region Request codes
        private const val REQUEST_ENABLE_BT = 414124124
        private const val REQUEST_ENABLE_GPS = 414124125

        //endregion
        //region SavedState
        private const val BT_INTERACTION_DELEGATE_BUNDLE = "BT_INTERACTION_DELEGATE_BUNDLE"
    }

}