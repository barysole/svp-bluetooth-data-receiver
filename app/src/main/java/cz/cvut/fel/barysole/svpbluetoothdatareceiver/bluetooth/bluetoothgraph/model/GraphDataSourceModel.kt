package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.model

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.DataSourceType

data class GraphDataSourceModel(
    val name: String?,
    val sourceType: DataSourceType?,
    val description: String?
)
