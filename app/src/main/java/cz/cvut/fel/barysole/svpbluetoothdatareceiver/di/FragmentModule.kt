package cz.cvut.fel.barysole.svpbluetoothdatareceiver.di

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(val fragment: Fragment) {

    @Provides
    fun providesFragment(): Fragment {
        return fragment
    }

}