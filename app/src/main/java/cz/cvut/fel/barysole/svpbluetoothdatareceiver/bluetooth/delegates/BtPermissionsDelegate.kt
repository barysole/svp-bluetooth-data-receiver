package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates

import android.Manifest.permission.*
import android.app.Activity
import android.os.Build
import androidx.fragment.app.Fragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R

open class BtPermissionsDelegate(private val fragment: Fragment) :
    BaseRtPermissionsDelegate(fragment) {

    override fun getPermissionNames(): Array<String> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(BLUETOOTH_SCAN, BLUETOOTH_CONNECT)
        } else {
            arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
        }
    }

    override fun getRationaleLight(permissionNames: Array<String>) =
        fragment.getString(R.string.bt_permission_rationale_light)

    override fun getRationaleHard(permissionNames: Array<String>) =
        fragment.getString(R.string.bt_permission_rationale_hard)

    override fun requestPermissions(permissions: Array<String>, requestCode: Int) =
        fragment.requestPermissions(permissions, requestCode)

}