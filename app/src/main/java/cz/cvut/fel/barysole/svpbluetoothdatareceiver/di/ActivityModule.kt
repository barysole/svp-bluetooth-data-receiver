package cz.cvut.fel.barysole.svpbluetoothdatareceiver.di

import android.app.Activity
import android.content.ContentResolver
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {
    @Provides
    fun activity(): Activity {
        return activity
    }

    @Provides
    fun appCompatActivity(): AppCompatActivity {
        return activity
    }

    @Provides
    fun contentResolver(): ContentResolver {
        return activity.contentResolver
    }
}