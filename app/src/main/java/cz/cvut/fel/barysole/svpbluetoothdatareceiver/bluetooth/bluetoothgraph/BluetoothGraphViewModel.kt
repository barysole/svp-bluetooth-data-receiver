package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.OpenableColumns
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.DataSourceType
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.GraphDataStatus
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.model.GraphDataSourceModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.livedata.SingleEventLiveData
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.parseBtMajorDeviceType
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.viewmodel.BaseViewModel
import java.io.*
import java.util.*
import javax.inject.Inject

class BluetoothGraphViewModel @Inject constructor(
    private val contentResolver: ContentResolver,
    private val currentFragment: Fragment?,
    private val activity: AppCompatActivity
) : BaseViewModel() {

    private var chosenDeviceMacAddress: String = ""

    //UUID of required device service
    private val serviceUUID = UUID.fromString("41721a3f-c501-4ed8-b57f-dc981fe20f4a")

    //todo: move to database
    //key is mac address
    private val serviceData = mutableMapOf<String, List<Pair<Float, Float>>>()

    var requestPairedDevicesCallback: (() -> Unit)? = null

    val chosenSourceLiveData = MutableLiveData<GraphDataSourceModel>()
    val isLoadingLiveData = MutableLiveData<Boolean>()
    val graphDataStatusLiveData = MutableLiveData<GraphDataStatus>()
    val graphDataLiveData = MutableLiveData<List<Pair<Float, Float>>>()

    //pair<MacAddress, UUID>
    val connectToDeviceServiceLiveData = SingleEventLiveData<Pair<String, UUID>>()

    override fun onStart() {
        graphDataStatusLiveData.value = GraphDataStatus.DATA_IS_EMPTY
    }

    fun onDeviceChosen(chosenDeviceMacAddress: String) {
        this.chosenDeviceMacAddress = chosenDeviceMacAddress
        requestPairedDevicesCallback?.invoke()
    }

    @SuppressLint("MissingPermission")
    fun onPairedDevicesRetrieve(pairedDevices: Set<BluetoothDevice>) {
        for (bluetoothDevice in pairedDevices) {
            if (bluetoothDevice.address == chosenDeviceMacAddress) {
                chosenSourceLiveData.value = GraphDataSourceModel(
                    bluetoothDevice.name ?: bluetoothDevice.address,
                    parseBtMajorDeviceType(bluetoothDevice.bluetoothClass.majorDeviceClass),
                    if (bluetoothDevice.name.isNullOrEmpty()) "" else bluetoothDevice.address
                )
                if (!serviceData[chosenDeviceMacAddress].isNullOrEmpty()) {
                    graphDataStatusLiveData.value = GraphDataStatus.DATA_IS_RECEIVED
                    graphDataLiveData.value = serviceData[chosenDeviceMacAddress]
                } else {
                    graphDataStatusLiveData.value = GraphDataStatus.DATA_IS_EMPTY
                }
                break
            }
        }
    }

    fun onConnectButtonClick() {
        isLoadingLiveData.value = true
        connectToDeviceServiceLiveData.value = Pair(chosenDeviceMacAddress, serviceUUID)
    }

    fun onRefreshButtonClick() {
        isLoadingLiveData.value = true
        connectToDeviceServiceLiveData.value = Pair(chosenDeviceMacAddress, serviceUUID)
    }

    //get nulls if connection establishing fails
    fun onConnectionEstablishingIsFinished(inputStream: InputStream?, outputStream: OutputStream?) {
        isLoadingLiveData.value = false
        if (inputStream != null) {
            graphDataStatusLiveData.value = GraphDataStatus.DATA_IS_RECEIVING
            isLoadingLiveData.value = true
            Thread {
                val receivedData = mutableListOf<Pair<Float, Float>>()
                try {
                    //AT THIS MOMENT WE READ JAVA(!) DATA TYPES FROM INPUT STREAM
                    val inputStreamReader = DataInputStream(inputStream)
                    val elementCount = inputStreamReader.readInt()
                    for (i in 0 until elementCount) {
                        //read x and y coordinates
                        receivedData.add(
                            Pair(
                                inputStreamReader.readFloat(),
                                inputStreamReader.readFloat()
                            )
                        )
                    }
                    serviceData[chosenDeviceMacAddress] = receivedData
                    graphDataStatusLiveData.postValue(GraphDataStatus.DATA_IS_RECEIVED)
                    graphDataLiveData.postValue(receivedData)
                } catch (e: RuntimeException) {
                    graphDataStatusLiveData.postValue(GraphDataStatus.ERROR_DURING_RECEIVING_PROCESS)
                }
                isLoadingLiveData.postValue(false)
            }.start()
        }
    }

    fun onGraphFileCreating(fileUri: Uri) {
        isLoadingLiveData.value = true
        try {
            contentResolver.openFileDescriptor(fileUri, "w")?.use {
                BufferedWriter(FileWriter(it.fileDescriptor)).use {
                    serviceData[chosenDeviceMacAddress]?.apply {
                        it.write((this.size).toString())
                        it.newLine()
                        for (coordinatesPair in this) {
                            it.write(coordinatesPair.first.toString() + " ");
                            it.write(coordinatesPair.second.toString());
                            it.newLine()
                        }
                    }
                }
                it.close()
            }
            graphDataStatusLiveData.value = GraphDataStatus.DATA_SUCCESEFULLY_SAVED
        } catch (e: FileNotFoundException) {
            graphDataStatusLiveData.postValue(GraphDataStatus.ERROR_DURING_RECEIVING_PROCESS)
        } catch (e: IOException) {
            graphDataStatusLiveData.postValue(GraphDataStatus.ERROR_DURING_RECEIVING_PROCESS)
        }
        isLoadingLiveData.value = false
    }

    fun onFileOpening(fileUri: Uri) {
        //todo: add exception handling
        isLoadingLiveData.value = true
        val savedData = mutableListOf<Pair<Float, Float>>()
        contentResolver.openInputStream(fileUri)?.use { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).use { reader ->
                var line: String? = reader.readLine()
                var coordinatesCount = 0;
                if (line != null) {
                    coordinatesCount = line.toInt()
                }
                for (counter in 0 until coordinatesCount) {
                    line = reader.readLine()
                    if (!line.isNullOrEmpty()) {
                        val stringTokenizer = StringTokenizer(line)
                        //read x and y coordinates
                        savedData.add(
                            Pair(
                                stringTokenizer.nextToken().toFloat(),
                                stringTokenizer.nextToken().toFloat()
                            )
                        )
                    }
                }
            }
        }
        //retrieving metadata from the file
        val cursor: Cursor? = contentResolver.query(
            fileUri, null, null, null, null, null
        )

        cursor?.use {
            if (it.moveToFirst()) {
                val displayNameColumnIndex = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                val displayName = if (displayNameColumnIndex >= 0) {
                    it.getString(displayNameColumnIndex)
                } else {
                    ""
                }
                chosenSourceLiveData.value =
                    GraphDataSourceModel(displayName, DataSourceType.TEXT_FILE, null)
            }
        }
        graphDataStatusLiveData.value = GraphDataStatus.DATA_SUCCESEFULLY_OPENED
        graphDataLiveData.value = savedData
        isLoadingLiveData.value = false
    }


}