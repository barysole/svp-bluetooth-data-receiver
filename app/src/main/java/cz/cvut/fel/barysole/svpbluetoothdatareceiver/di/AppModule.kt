package cz.cvut.fel.barysole.svpbluetoothdatareceiver.di

import android.os.Handler
import android.os.Looper
import androidx.core.os.HandlerCompat
import dagger.Module
import dagger.Provides

@Module
class AppModule() {

    @Provides
    fun mainThreadHandler(): Handler {
        return HandlerCompat.createAsync(Looper.getMainLooper())
    }

}