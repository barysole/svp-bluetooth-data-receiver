package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model

import android.bluetooth.BluetoothDevice
import android.os.ParcelUuid
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.BtDeviceListItem

data class BTDeviceModel(
    val uuids: Array<ParcelUuid>?,
    var isPaired: Boolean,
    var isPairing: Boolean = false,
    var isChosen: Boolean = false,
    val name: String?,
    val majorDeviceType: Int?,
    val macAddress: String,
    val bluetoothDeviceFull: BluetoothDevice
) : BtDeviceListItem