package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.BOND_BONDED
import androidx.lifecycle.MutableLiveData
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.BtDeviceListItem
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.enums.BTGroupInfoType
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model.BTDeviceModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model.BTGroupInfo
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.livedata.SingleEventLiveData
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.UserError
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.viewmodel.BaseViewModel
import javax.inject.Inject

//todo: move BTDeviceModel constructing to BluetoothInteractionDelegate
class BluetoothDevicesViewModel @Inject constructor() : BaseViewModel() {
    private var discoveredDeviceGroupHeader =
        BTGroupInfo("Discovered devices:", BTGroupInfoType.DISCOVERED_DEVICES, false)

    //todo: move position control to adapter?
    private var discoveredDevicesGroupHeaderPosition: Int = 0

    private val knownDiscoveredDeviceList = mutableSetOf<BtDeviceListItem>()

    //temporal solution
    //todo: move to app database
    private val pairedDevices = arrayListOf<BTDeviceModel>()
    private val discoveredDevices = arrayListOf<BTDeviceModel>()

    val changeItemLiveData = MutableLiveData<Int>()
    val userErrorLiveData = SingleEventLiveData<UserError>()
    val newBtDeviceLiveData = SingleEventLiveData<BtDeviceListItem>()
    val requestPairingLiveData = SingleEventLiveData<BluetoothDevice>()
    val cachedDeviceListLiveData = MutableLiveData<ArrayList<BtDeviceListItem>>()
    val chosenDeviceLiveData = MutableLiveData<BTDeviceModel>()
    var requestPairedDevicesCallback: (() -> Unit)? = null
    var requestStartDiscoveringCallback: (() -> Unit)? = null

    override fun onStart() {
        //request paired devices
        val deviceList = arrayListOf<BtDeviceListItem>()
        deviceList.add(BTGroupInfo("Paired devices:", BTGroupInfoType.PAIRED_DEVICES))
        discoveredDevicesGroupHeaderPosition = deviceList.size
        deviceList.add(discoveredDeviceGroupHeader)
        cachedDeviceListLiveData.value = deviceList
        requestPairedDevicesCallback?.invoke()
    }

    @SuppressLint("MissingPermission")
    fun onPairedDevicesRetrieve(newPairedDevices: Set<BluetoothDevice>) {
        val previousSelectedDevice =
            pairedDevices.find { bluetoothDevice -> bluetoothDevice.isChosen }
        pairedDevices.clear()
        for (bluetoothDevice in newPairedDevices) {
            val pairedBtDevice = BTDeviceModel(
                uuids = bluetoothDevice.uuids,
                name = bluetoothDevice.name,
                macAddress = bluetoothDevice.address,
                isPaired = bluetoothDevice.bondState == BOND_BONDED,
                isChosen = bluetoothDevice.address == previousSelectedDevice?.macAddress,
                majorDeviceType = bluetoothDevice.bluetoothClass.majorDeviceClass,
                bluetoothDeviceFull = bluetoothDevice
            )
            pairedDevices.add(pairedBtDevice)
        }
        recreateList()
    }

    @SuppressLint("MissingPermission")
    fun onNewDeviceDiscover(newDevice: BluetoothDevice) {
        //IGNORE DEVICES WITHOUT NAME
        if (!newDevice.name.isNullOrEmpty()) {
            val newBtDevice = BTDeviceModel(
                uuids = newDevice.uuids,
                name = newDevice.name,
                macAddress = newDevice.address,
                isPaired = newDevice.bondState == BOND_BONDED,
                majorDeviceType = newDevice.bluetoothClass.majorDeviceClass,
                bluetoothDeviceFull = newDevice
            )
            //if does not exist
            if (knownDiscoveredDeviceList.add(newBtDevice) && !newBtDevice.isPaired) {
                newBtDeviceLiveData.value = newBtDevice
                discoveredDevices.add(newBtDevice)
            }
        }
    }

    fun onDestroy() {
        //save discovered devices
        cachedDeviceListLiveData.value = getBtDeviceList()
    }

    fun onBluetoothDeviceItemClick(bluetoothDevice: BTDeviceModel, position: Int) {
        if (!bluetoothDevice.isPaired) {
            bluetoothDevice.isPairing = true
            requestPairingLiveData.value = bluetoothDevice.bluetoothDeviceFull
        } else {
            for ((index, device) in pairedDevices.withIndex()) {
                if (device.isChosen) {
                    device.isChosen = false
                    changeItemLiveData.value = index + 1;
                }
            }
            bluetoothDevice.isChosen = true
            changeItemLiveData.value = position
            chosenDeviceLiveData.value = bluetoothDevice
        }
    }

    fun onFileChoosesAsDataSource() {
        for ((index, device) in pairedDevices.withIndex()) {
            if (device.isChosen) {
                device.isChosen = false
                changeItemLiveData.value = index + 1;
            }
        }
    }

    fun onBtDiscoveringLoaderChanges(isLoading: Boolean) {
        discoveredDeviceGroupHeader.isLoading = isLoading
        changeItemLiveData.value = discoveredDevicesGroupHeaderPosition
    }

    fun onUpdateBTGroup(groupInfoType: BTGroupInfoType) {
        when (groupInfoType) {
            BTGroupInfoType.DISCOVERED_DEVICES -> {
                knownDiscoveredDeviceList.clear()
                discoveredDevices.clear()
                recreateList()
                requestStartDiscoveringCallback?.invoke()
            }
            BTGroupInfoType.PAIRED_DEVICES -> {
                requestPairedDevicesCallback?.invoke()
            }
        }
    }

    fun onBondingStart(bluetoothDevice: BluetoothDevice) {
        val btDevice =
            discoveredDevices.first { btDeviceListItem -> btDeviceListItem.bluetoothDeviceFull == bluetoothDevice }
        btDevice.isPairing = true
        recreateList()
    }

    @SuppressLint("MissingPermission")
    fun onBondFinished(bluetoothDevice: BluetoothDevice) {
        val btDevice =
            discoveredDevices.first { btDeviceListItem -> btDeviceListItem.bluetoothDeviceFull == bluetoothDevice }
        btDevice.isPairing = false
        if (bluetoothDevice.bondState == BOND_BONDED) {
            discoveredDevices.remove(btDevice)
            pairedDevices.add(btDevice)
        }
        recreateList()
    }

    private fun recreateList() {
        cachedDeviceListLiveData.value = getBtDeviceList()
    }

    private fun getBtDeviceList(): ArrayList<BtDeviceListItem> {
        val deviceList = arrayListOf<BtDeviceListItem>()
        deviceList.add(BTGroupInfo("Paired devices:", BTGroupInfoType.PAIRED_DEVICES))
        for (pairedDevice: BtDeviceListItem in pairedDevices) {
            deviceList.add(pairedDevice)
        }
        discoveredDevicesGroupHeaderPosition = deviceList.size
        deviceList.add(discoveredDeviceGroupHeader)
        for (discoveredDevice: BtDeviceListItem in discoveredDevices) {
            deviceList.add(discoveredDevice)
        }
        return deviceList
    }

}