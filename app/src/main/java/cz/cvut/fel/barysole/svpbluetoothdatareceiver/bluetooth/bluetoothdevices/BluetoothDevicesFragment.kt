package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.BtDeviceListAdapter
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.BtDeviceListItem
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.ItemDecoration
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model.BTDeviceModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates.BluetoothInteractionDelegate
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.FragmentModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.mainactivity.MainActivity
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.UserError
import javax.inject.Inject

class BluetoothDevicesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<BluetoothDevicesViewModel> { viewModelFactory }

    @Inject
    lateinit var bluetoothInteractionDelegate: BluetoothInteractionDelegate

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: BtDeviceListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        //todo: implement callback in bt delegate
        //registerForActivityResult()
        (activity as MainActivity).bluetoothComponent.bluetoothDevicesComponent()
            .create(FragmentModule(this)).inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bluetoothInteractionDelegate.onCreate(savedInstanceState)
        setFragmentResultListener(BluetoothGraphFragment.FILE_IS_CHOSEN_AS_DATA_SOURCE_KEY) { _, _ ->
            viewModel.onFileChoosesAsDataSource()
        }
    }

    //todo: load changes after screen rotation
    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_devices, container, false)
        recyclerView = view.findViewById(R.id.fragment_devices_recycle_view)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.addItemDecoration(ItemDecoration(resources.getDimensionPixelOffset(R.dimen.bt_devices_item_offset)))
        adapter = BtDeviceListAdapter()
        recyclerView.adapter =
            adapter.apply {
                onDeviceItemClickListener = { btDeviceModel, position ->
                    viewModel.onBluetoothDeviceItemClick(btDeviceModel, position)
                }
                onUpdateGroupClickListener = { group ->
                    viewModel.onUpdateBTGroup(group)
                }
            }
        viewModel.apply {
            cachedDeviceListLiveData.observe({ viewLifecycleOwner.lifecycle }, ::setBtDeviceItems)
            newBtDeviceLiveData.observe({ viewLifecycleOwner.lifecycle }, ::pushBtDeviceItemToEnd)
            changeItemLiveData.observe({ viewLifecycleOwner.lifecycle }, ::changeBtDeviceAt)
            requestPairingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { device -> bluetoothInteractionDelegate.pairWithDevice(device) })
            chosenDeviceLiveData.observe({ viewLifecycleOwner.lifecycle }, ::broadcastChosenDevice)
            requestPairedDevicesCallback =
                { bluetoothInteractionDelegate.retrievePairedDeviceList() }
            requestStartDiscoveringCallback =
                { bluetoothInteractionDelegate.startDeviceDiscovery() }
        }
        bluetoothInteractionDelegate.apply {
            onShowErrorListener = ::showError
            onPairedDeviceRetrievedListener =
                { pairedDevices -> viewModel.onPairedDevicesRetrieve(pairedDevices) }
            onNewDiscoveredDeviceListener = { newDevice ->
                viewModel.onNewDeviceDiscover(newDevice)
            }
            onDiscoveringStateChangeListener = { isLoading -> viewModel.onBtDiscoveringLoaderChanges(isLoading) }
            onDeviceBondingStartListener =
                { device -> viewModel.onBondingStart(device) }
            onDeviceBondFinishedListener =
                { device -> viewModel.onBondFinished(device) }
        }
        viewModel.start()
        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bluetoothInteractionDelegate.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        bluetoothInteractionDelegate.onDestroy()
        viewModel.onDestroy()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        bluetoothInteractionDelegate.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        bluetoothInteractionDelegate.onActivityResult(requestCode, resultCode, data)
    }

    //todo: move common class
    private fun showError(userError: UserError) {
        Snackbar.make(requireView(), userError.message, Snackbar.LENGTH_LONG).show()
    }

    private fun setBtDeviceItems(items: ArrayList<BtDeviceListItem>) {
        adapter.setItems(items)
    }

    private fun pushBtDeviceItemToEnd(items: BtDeviceListItem) {
        adapter.pushLast(items)
    }

    private fun changeBtDeviceAt(devicePosition: Int) {
        adapter.notifyItemChanged(devicePosition)
    }

    private fun broadcastChosenDevice(btDeviceModel: BTDeviceModel) {
        //for device fragment
        setFragmentResult(
            BT_CHOSEN_DEVICE_MAC_AVAILABLE_KEY, bundleOf(
                BT_DEVICE_CHOSEN_DEVICE_MAC_ADDRESS to btDeviceModel.macAddress
            )
        )
        setFragmentResult(
            BT_CHOSEN_DEVICE_NAME_AVAILABLE_KEY, bundleOf(
                BT_DEVICE_CHOSEN_DEVICE_NAME to btDeviceModel.name
            )
        )
    }

    companion object {
        const val BT_CHOSEN_DEVICE_MAC_AVAILABLE_KEY = "BT_CHOSEN_DEVICE_MAC_AVAILABLE_KEY"
        const val BT_CHOSEN_DEVICE_NAME_AVAILABLE_KEY = "BT_CHOSEN_DEVICE_NAME_AVAILABLE_KEY"
        const val BT_DEVICE_CHOSEN_DEVICE_NAME = "BT_FRAGMENT_CHOSEN_DEVICE_NAME"
        const val BT_DEVICE_CHOSEN_DEVICE_MAC_ADDRESS = "BT_FRAGMENT_CHOSEN_DEVICE_MAC_ADDRESS"
    }

}