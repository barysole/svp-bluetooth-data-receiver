package cz.cvut.fel.barysole.svpbluetoothdatareceiver

import android.app.Application
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.AppComponent
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.DaggerAppComponent

open class BTDataReceiverApp : Application() {

    // Instance of the AppComponent that will be used by all the Activities in the project
    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): AppComponent {
        // Creates an instance of AppComponent using its Factory constructor
        // We pass the applicationContext that will be used as Context in the graph
        return DaggerAppComponent.factory().create(applicationContext)
    }

}