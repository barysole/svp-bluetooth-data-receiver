package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment.Companion.CHOSEN_FILE_NAME
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment.Companion.FILE_IS_CHOSEN_AS_DATA_SOURCE_KEY
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.adapters.BluetoothMainVPAdapter
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.mainactivity.MainActivity
import javax.inject.Inject

class BluetoothMainFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<BluetoothMainViewModel> { viewModelFactory }

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager2

    companion object {
        fun newInstance() = BluetoothMainFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).bluetoothComponent.bluetoothMainComponent()
            .create().inject(this)
        childFragmentManager.setFragmentResultListener(BluetoothDevicesFragment.BT_CHOSEN_DEVICE_NAME_AVAILABLE_KEY, this) { _, bundle ->
            val chosenDeviceName = bundle.getString(BluetoothDevicesFragment.BT_DEVICE_CHOSEN_DEVICE_NAME).toString()
            viewModel.onSourceChosen(chosenDeviceName)
        }
        childFragmentManager.setFragmentResultListener(BluetoothGraphFragment.CHOSEN_FILE_NAME_KEY, this) { _, bundle ->
            val chosenFileName = bundle.getString(CHOSEN_FILE_NAME).toString()
            viewModel.onSourceChosen(chosenFileName)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bluetooth_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout = view.findViewById(R.id.tabLayout)
        viewPager = view.findViewById(R.id.viewPager)
        viewPager.adapter = BluetoothMainVPAdapter(this)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.devicesText)
                1 -> viewModel.chosenSourceNameLiveData.value?: getString(R.string.no_device_chosen)
                else -> {
                    "undefined"
                }
            }
        }.attach()
        viewModel.apply {
            chosenSourceNameLiveData.observe({ viewLifecycleOwner.lifecycle }, ::renameDeviceTab)
        }
    }

    private fun renameDeviceTab(deviceName: String) {
        tabLayout.getTabAt(1)?.text = deviceName
    }

}