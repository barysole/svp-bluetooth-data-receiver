package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class BluetoothMainViewModel @Inject constructor(): ViewModel() {

    val chosenSourceNameLiveData = MutableLiveData<String>()

    fun onSourceChosen(chosenDeviceName: String) {
        chosenSourceNameLiveData.value = chosenDeviceName
    }

}