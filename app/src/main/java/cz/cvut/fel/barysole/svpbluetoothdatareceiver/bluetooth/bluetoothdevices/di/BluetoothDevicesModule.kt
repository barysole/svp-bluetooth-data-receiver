package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.di

import androidx.lifecycle.ViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BluetoothDevicesModule {

    @Binds
    @IntoMap
    @ViewModelKey(BluetoothDevicesViewModel::class)
    abstract fun bindViewModel(viewModel: BluetoothDevicesViewModel): ViewModel

}