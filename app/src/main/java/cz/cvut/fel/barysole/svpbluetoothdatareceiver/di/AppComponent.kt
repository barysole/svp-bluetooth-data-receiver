package cz.cvut.fel.barysole.svpbluetoothdatareceiver.di

import android.content.Context
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.di.BluetoothComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ViewModelBuilderModule::class,
        AppModule::class
    ]
)
@Singleton
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }

    fun bluetoothComponent(): BluetoothComponent.Factory

}