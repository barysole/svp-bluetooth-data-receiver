package cz.cvut.fel.barysole.svpbluetoothdatareceiver.mainactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.BTDataReceiverApp
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.BluetoothMainFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.ActivityModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.di.BluetoothComponent

class MainActivity : AppCompatActivity() {

    lateinit var bluetoothComponent: BluetoothComponent
    fun component() = bluetoothComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        bluetoothComponent = (application as BTDataReceiverApp).appComponent.bluetoothComponent()
            .create(ActivityModule(this))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, BluetoothMainFragment.newInstance())
                .commitNow()
        }
    }
}