package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.di

import androidx.lifecycle.ViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.BluetoothMainViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BluetoothMainModule {

    @Binds
    @IntoMap
    @ViewModelKey(BluetoothMainViewModel::class)
    abstract fun bindViewModel(viewModel: BluetoothMainViewModel): ViewModel

}