package cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.adapter

import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.ImageView

//For imageview only
class OnTouchIndicatorListener(private val standardBackground: Drawable?, private val onTouchBackground: Drawable?) : OnTouchListener {

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val view: ImageView = v as ImageView
                view.background = onTouchBackground
                view.invalidate()
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                val view: ImageView = v as ImageView
                view.background = standardBackground
                view.invalidate()
            }
        }
        return false
    }

}