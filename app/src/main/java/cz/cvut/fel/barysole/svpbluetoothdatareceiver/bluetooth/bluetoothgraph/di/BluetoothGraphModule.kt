package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.di

import androidx.lifecycle.ViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphViewModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BluetoothGraphModule {

    @Binds
    @IntoMap
    @ViewModelKey(BluetoothGraphViewModel::class)
    abstract fun bindViewModel(viewModel: BluetoothGraphViewModel): ViewModel

}