package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.enums

enum class BluetoothActionEnum {
    START_DISCOVERY,
    STOP_DISCOVERY,
    RETRIEVE_PAIRED_DEVICES,
    GET_CONNECTION_TO_BT_DEVICE
}