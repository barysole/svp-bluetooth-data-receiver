package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import java.util.*

/**
 * Base class for Runtime Permissions checking.
 */
abstract class BaseRtPermissionsDelegate(private val fragment: Fragment) {

    var listener: ((granted: Boolean) -> Unit)? = null
    private var lightRationaleAlertDialog: AlertDialog? = null
    private var hardRationaleAlertDialog: AlertDialog? = null

    protected var requestInProgress: Boolean = false

    open fun onCreate(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState?.getBundle(SS_PERMISSION_DELEGATE_BUNDLE) ?: return
        requestInProgress = bundle.getBoolean(SS_REQUEST_IN_PROGRESS)
    }

    open fun onSaveInstanceState(outState: Bundle) {
        val bundle = Bundle()
        bundle.putBoolean(SS_REQUEST_IN_PROGRESS, requestInProgress)
        bundle.putBoolean(SS_LIGHT_RATIONALE_SHOWN, lightRationaleAlertDialog != null)
        bundle.putBoolean(SS_HARD_RATIONALE_SHOWN, hardRationaleAlertDialog != null)
        outState.putBundle(SS_PERMISSION_DELEGATE_BUNDLE, bundle)
    }

    open fun onDestroy() {
        dismissRationaleHardDialog()
        dismissRationaleLightDialog()
    }

    //returns false if permission request in progress
    //returns true if permissions are granted
    fun checkPermissions(): Boolean {
        if (requestInProgress) {
            return false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissions = getPermissionNames()
            val grantedPermissions = ArrayList<String>()
            val notGrantedRationalePermissions = ArrayList<String>()
            val notGrantedPermissions = ArrayList<String>()
            for (permission in permissions) {
                when {
                    ActivityCompat.checkSelfPermission(
                        fragment.requireContext(),
                        permission
                    ) == PackageManager.PERMISSION_GRANTED -> {
                        grantedPermissions.add(permission)
                    }
                    fragment.shouldShowRequestPermissionRationale(permission) -> {
                        notGrantedRationalePermissions.add(permission)
                    }
                    else -> {
                        notGrantedPermissions.add(permission)
                    }
                }
            }
            if (notGrantedPermissions.isNotEmpty()) {
                requestPermissionsInternal(getPermissionNames(), RC_PERMISSION)
                return false;
            } else if (notGrantedRationalePermissions.isNotEmpty()) {
                showRationaleLightDialog(notGrantedRationalePermissions.toTypedArray())
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestPermissionsInternal(permissions: Array<String>, requestCode: Int) {
        requestInProgress = true
        requestPermissions(permissions, requestCode)
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == RC_PERMISSION || requestCode == RC_PERMISSION_AFTER_RATIONALE) {
            requestInProgress = false
            val grantedPermissions = ArrayList<String>()
            val notGrantedHardPermissions = ArrayList<String>()
            val notGrantedPermissions = ArrayList<String>()
            if (grantResults.size == getPermissionNames().size) {
                for (i in permissions.indices) {
                    val grantResult = grantResults[i]
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        grantedPermissions.add(permissions[i])
                        continue
                    }
                    if (!fragment.shouldShowRequestPermissionRationale(permissions[i]) && requestCode == RC_PERMISSION) {
                        if (grantResult == PackageManager.PERMISSION_DENIED) {
                            notGrantedHardPermissions.add(permissions[i])
                            continue
                        }
                    }
                    notGrantedPermissions.add(permissions[i])
                }
                if (notGrantedPermissions.isNotEmpty()) {
                    listener?.invoke(false)
                } else if (notGrantedHardPermissions.isNotEmpty()) {
                    showRationaleHardDialog(notGrantedHardPermissions.toTypedArray())
                } else {
                    listener?.invoke(true)
                }
                return
            }
            listener?.invoke(false)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun showRationaleLightDialog(permissionNames: Array<String>) {
        dismissRationaleHardDialog()
        if (lightRationaleAlertDialog != null) {
            return
        }
        lightRationaleAlertDialog = AlertDialog.Builder(fragment.requireContext())
            .setMessage(getRationaleLight(permissionNames))
            .setPositiveButton(R.string.common_rt_permissions_rationale_light_continue) { _, _ ->
                requestPermissionsInternal(
                    permissionNames,
                    RC_PERMISSION_AFTER_RATIONALE
                )
            }
            .setNegativeButton(R.string.common_rt_permissions_rationale_light_cancel) { _, _ ->
                listener?.invoke(
                    false
                )
            }
            .setOnDismissListener { dismissRationaleLightDialog() }
            .setCancelable(false)
            .show()
    }

    private fun dismissRationaleLightDialog() {
        if (lightRationaleAlertDialog != null) {
            lightRationaleAlertDialog!!.dismiss()
            lightRationaleAlertDialog = null
        }
    }

    private fun showRationaleHardDialog(permissionNames: Array<String>) {
        dismissRationaleLightDialog()
        if (hardRationaleAlertDialog != null) {
            return
        }
        hardRationaleAlertDialog = AlertDialog.Builder(fragment.requireContext())
            .setMessage(getRationaleHard(permissionNames))
            .setPositiveButton(R.string.common_rt_permissions_rationale_hard_continue) { _, _ ->
                navigateToAppSettings(
                    fragment.requireActivity()
                )
            }
            .setNegativeButton(R.string.common_rt_permissions_rationale_hard_cancel) { _, _ ->
                listener?.invoke(
                    false
                )
            }
            .setOnDismissListener { dismissRationaleHardDialog() }
            .setCancelable(false)
            .show()
    }

    private fun dismissRationaleHardDialog() {
        hardRationaleAlertDialog?.let {
            it.dismiss()
            hardRationaleAlertDialog = null
        }
    }

    private fun navigateToAppSettings(activity: Activity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.parse("package:" + activity.packageName)
        activity.startActivity(intent)
    }

    /**
     * Returns permission names in this delegate
     */
    protected abstract fun getPermissionNames(): Array<String>

    /**
     * Message shown if user reject permissions without "never ask again" flag
     */
    protected abstract fun getRationaleLight(permissionNames: Array<String>): String

    /**
     * Message shown if user reject permissions with "never ask again" flag
     */
    protected abstract fun getRationaleHard(permissionNames: Array<String>): String

    /**
     * Release system Api permission call
     */
    protected abstract fun requestPermissions(permissions: Array<String>, requestCode: Int)

    companion object {
        //region Request codes
        private const val RC_PERMISSION_AFTER_RATIONALE = 102
        private const val RC_PERMISSION = 103

        //endregion
        //region SavedState
        private const val SS_PERMISSION_DELEGATE_BUNDLE = "SS_PERMISSION_DELEGATE_BUNDLE"
        private const val SS_REQUEST_IN_PROGRESS = "SS_REQUEST_IN_PROGRESS"
        private const val SS_LIGHT_RATIONALE_SHOWN = "SS_LIGHT_RATIONALE_SHOWN"
        private const val SS_HARD_RATIONALE_SHOWN = "SS_HARD_RATIONALE_SHOWN"
    }
}
