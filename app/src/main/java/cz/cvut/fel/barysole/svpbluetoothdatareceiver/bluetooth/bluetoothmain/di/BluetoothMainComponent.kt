package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.di

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.BluetoothMainFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.LocationModule
import dagger.Subcomponent

@Subcomponent(modules = [BluetoothMainModule::class, LocationModule::class])
interface BluetoothMainComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): BluetoothMainComponent
    }

    fun inject(fragment: BluetoothMainFragment)

}