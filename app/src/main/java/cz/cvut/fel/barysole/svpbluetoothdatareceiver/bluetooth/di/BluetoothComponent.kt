package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.di

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.di.BluetoothDevicesComponent
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.di.BluetoothGraphComponent
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.di.BluetoothMainComponent
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.ActivityModule
import dagger.Subcomponent

@Subcomponent(
    modules = [
        ActivityModule::class
    ]
)
interface BluetoothComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(activityModule: ActivityModule): BluetoothComponent
    }

    fun bluetoothMainComponent(): BluetoothMainComponent.Factory
    fun bluetoothGraphComponent(): BluetoothGraphComponent.Factory
    fun bluetoothDevicesComponent(): BluetoothDevicesComponent.Factory

}