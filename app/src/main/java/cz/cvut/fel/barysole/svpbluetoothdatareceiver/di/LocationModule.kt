package cz.cvut.fel.barysole.svpbluetoothdatareceiver.di

import android.content.Context
import android.location.LocationManager
import dagger.Module
import dagger.Provides

@Module
class LocationModule {

    @Provides
    fun provideLocationManager(context: Context): LocationManager {
        return context.getSystemService(LocationManager::class.java);
    }

}