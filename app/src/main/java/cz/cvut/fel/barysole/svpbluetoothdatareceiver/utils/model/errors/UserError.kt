package cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors

interface UserError {

    val message: String

    companion object {
        val NO_ERROR: UserError = object : UserError {
            override val message = ""
        }
    }
}
