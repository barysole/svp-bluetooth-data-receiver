package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter.BtDeviceListItem
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.enums.BTGroupInfoType

data class BTGroupInfo(
    val name: String,
    val byBTGroupInfoType: BTGroupInfoType,
    var isLoading: Boolean = false
) : BtDeviceListItem
