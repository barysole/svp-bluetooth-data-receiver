package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.di

import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.di.BluetoothModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.FragmentModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.LocationModule
import dagger.Subcomponent

@Subcomponent(modules = [BluetoothGraphModule::class, BluetoothModule::class, LocationModule::class, FragmentModule::class])
interface BluetoothGraphComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(fragmentModule: FragmentModule): BluetoothGraphComponent
    }

    fun inject(fragment: BluetoothGraphFragment)

}