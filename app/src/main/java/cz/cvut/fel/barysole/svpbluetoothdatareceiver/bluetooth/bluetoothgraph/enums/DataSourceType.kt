package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums

enum class DataSourceType {
    TEXT_FILE,
    COMPUTER,
    AUDIO_VIDEO,
    PHONE,
    UNDEFINED
}