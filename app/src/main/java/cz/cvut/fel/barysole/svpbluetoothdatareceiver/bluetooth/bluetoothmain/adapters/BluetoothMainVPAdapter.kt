package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothmain.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.BluetoothGraphFragment

class BluetoothMainVPAdapter(viewPagerFragment: Fragment) : FragmentStateAdapter(viewPagerFragment) {

    override fun getItemCount(): Int {
        return 2;
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> BluetoothDevicesFragment()
            1 -> BluetoothGraphFragment()
            else -> {
                throw RuntimeException("Undefined fragment on position $position")
            }
        }
    }

}