package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesFragment
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.BluetoothDevicesFragment.Companion.BT_DEVICE_CHOSEN_DEVICE_MAC_ADDRESS
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.DataSourceType
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.enums.GraphDataStatus
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothgraph.model.GraphDataSourceModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates.BluetoothInteractionDelegate
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.delegates.FileSystemInteractionDelegate
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.di.FragmentModule
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.mainactivity.MainActivity
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.adapter.OnTouchIndicatorListener
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors.UserError
import java.util.*
import javax.inject.Inject


class BluetoothGraphFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var bluetoothInteractionDelegate: BluetoothInteractionDelegate

    @Inject
    lateinit var fileSystemInteractionDelegate: FileSystemInteractionDelegate

    private val viewModel by viewModels<BluetoothGraphViewModel> { viewModelFactory }

    private lateinit var dataSourceName: TextView
    private lateinit var dataSourceAdditionalInfo: TextView
    private lateinit var dataSourceDescription: TextView
    private lateinit var dataSourceIcon: ImageView
    private lateinit var dataSourceHolder: ViewGroup
    private lateinit var connectionButton: Button
    private lateinit var loaderHolder: ViewGroup
    private lateinit var loaderIcon: ImageView
    private lateinit var graphDataStatus: TextView
    private lateinit var lineChart: LineChart
    private lateinit var saveDataBtn: Button
    private lateinit var openDataBtn: Button
    private lateinit var refreshBtn: ImageView

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as MainActivity).bluetoothComponent.bluetoothGraphComponent()
            .create(FragmentModule(this)).inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(BluetoothDevicesFragment.BT_CHOSEN_DEVICE_MAC_AVAILABLE_KEY) { _, bundle ->
            //todo: implement by using id in database
            val chosenDeviceMacAddress =
                bundle.getString(BT_DEVICE_CHOSEN_DEVICE_MAC_ADDRESS).toString()
            viewModel.onDeviceChosen(chosenDeviceMacAddress)
        }
        bluetoothInteractionDelegate.onCreate(savedInstanceState)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_graph, container, false)
        dataSourceName = view.findViewById(R.id.dataSourceName)
        dataSourceAdditionalInfo = view.findViewById(R.id.dataSourceAdditionalInfo)
        dataSourceDescription = view.findViewById(R.id.dataSourceDescription)
        dataSourceIcon = view.findViewById(R.id.dataSourceIcon)
        dataSourceHolder = view.findViewById(R.id.dataSourceHolder)
        connectionButton = view.findViewById(R.id.connectToDeviceButton)
        loaderHolder = view.findViewById(R.id.loaderHolder)
        loaderIcon = view.findViewById(R.id.loaderIcon)
        graphDataStatus = view.findViewById(R.id.graphDataStatus)
        lineChart = view.findViewById(R.id.lineChart)
        saveDataBtn = view.findViewById(R.id.saveDataButton)
        openDataBtn = view.findViewById(R.id.openSavedDataButton)
        refreshBtn = view.findViewById(R.id.refreshButton)
        bluetoothInteractionDelegate.apply {
            onShowErrorListener = ::showError
            onPairedDeviceRetrievedListener =
                { pairedDevices -> viewModel.onPairedDevicesRetrieve(pairedDevices) }
            onConnectionResultListener =
                { ioPair ->
                    viewModel.onConnectionEstablishingIsFinished(
                        ioPair?.first,
                        ioPair?.second
                    )
                }
        }
        viewModel.apply {
            chosenSourceLiveData.observe({ viewLifecycleOwner.lifecycle }, ::showDataSourceInfo)
            requestPairedDevicesCallback =
                { bluetoothInteractionDelegate.retrievePairedDeviceList() }
            isLoadingLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { isLoading -> setLoading(isLoading) })
            connectToDeviceServiceLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { deviceMacAndUUIDPair ->
                    bluetoothInteractionDelegate.getConnectionToDeviceService(
                        deviceMacAndUUIDPair.first,
                        deviceMacAndUUIDPair.second
                    )
                })
            graphDataStatusLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { dataStatus -> applyDataStatus(dataStatus) })
            graphDataLiveData.observe(
                { viewLifecycleOwner.lifecycle },
                { data -> drawData(data) })
            start()
        }
        connectionButton.setOnClickListener {
            viewModel.onConnectButtonClick()
        }
        //todo: move to separate view
        val standardLoaderBG =
            AppCompatResources.getDrawable(requireContext(), R.drawable.ic_circle_outline_white)
        refreshBtn.setOnTouchListener(
            OnTouchIndicatorListener(
                standardLoaderBG,
                AppCompatResources.getDrawable(requireContext(), R.drawable.ic_circle_outline_gray)
            )
        )
        fileSystemInteractionDelegate.apply {
            onShowErrorListener = ::showError
            onFileOpenListener = { uri -> viewModel.onFileOpening(uri) }
            onFileCreateListener = { uri -> viewModel.onGraphFileCreating(uri) }
        }
        refreshBtn.setOnClickListener {
            viewModel.onRefreshButtonClick()
        }
        saveDataBtn.setOnClickListener {
            fileSystemInteractionDelegate.createTextFile(
                "GraphData - " + Calendar.getInstance().time
            )
        }
        openDataBtn.setOnClickListener {
            fileSystemInteractionDelegate.openTextFile()
        }
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bluetoothInteractionDelegate.checkPausedAction(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bluetoothInteractionDelegate.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        bluetoothInteractionDelegate.onDestroy()
        fileSystemInteractionDelegate.onDestroy()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        bluetoothInteractionDelegate.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        bluetoothInteractionDelegate.onActivityResult(requestCode, resultCode, data)
        fileSystemInteractionDelegate.onActivityResult(requestCode, resultCode, data)
    }

    private fun showError(userError: UserError) {
        activity?.apply {
            Snackbar.make(this.findViewById(android.R.id.content), userError.message, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun showDataSourceInfo(dataSource: GraphDataSourceModel) {
        dataSourceHolder.visibility = VISIBLE
        if (dataSource.sourceType != DataSourceType.TEXT_FILE) {
            connectionButton.visibility = VISIBLE
        }
        dataSourceName.text = dataSource.name ?: ""
        dataSourceAdditionalInfo.text = dataSource.description ?: ""
        //todo: move to utils
        if (dataSource.sourceType == DataSourceType.COMPUTER) {
            dataSourceIcon.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_computer_24
                )
            )
            dataSourceDescription.text = getString(R.string.data_source_current_device)
        } else if (dataSource.sourceType ==
            DataSourceType.AUDIO_VIDEO
        ) {
            dataSourceIcon.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_headphones_24
                )
            )
            dataSourceDescription.text = getString(R.string.data_source_current_device)
        } else if (dataSource.sourceType ==
            DataSourceType.PHONE
        ) {
            dataSourceIcon.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_smartphone_24
                )
            )
            dataSourceDescription.text = getString(R.string.data_source_current_device)
        } else if (dataSource.sourceType ==
            DataSourceType.TEXT_FILE
        ) {
            dataSourceIcon.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_file_24
                )
            )
            dataSourceDescription.text = getString(R.string.data_source_file)
            broadcastChosenFile(dataSource.name ?: "")
        } else {
            dataSourceIcon.setImageDrawable(
                AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_bluetooth_24
                )
            )
            dataSourceDescription.text = getString(R.string.data_source_current_device)
        }
    }

    private fun setLoading(isLoading: Boolean) {
        if (isLoading) {
            loaderHolder.visibility = VISIBLE
            (loaderIcon.drawable as AnimationDrawable).start()
        } else {
            (loaderIcon.drawable as AnimationDrawable).stop()
            loaderHolder.visibility = GONE
        }
    }

    private fun applyDataStatus(dataStatus: GraphDataStatus) {
        graphDataStatus.visibility = VISIBLE
        when (dataStatus) {
            GraphDataStatus.DATA_IS_EMPTY -> {
                graphDataStatus.text = ""
                lineChart.visibility = GONE
                refreshBtn.visibility = GONE
                saveDataBtn.isEnabled = false
            }
            GraphDataStatus.DATA_IS_RECEIVED -> {
                graphDataStatus.text = getString(R.string.receive_data_success)
                saveDataBtn.isEnabled = true
                connectionButton.visibility = GONE
                refreshBtn.visibility = VISIBLE
            }
            GraphDataStatus.ERROR_DURING_RECEIVING_PROCESS -> {
                graphDataStatus.text = getString(R.string.receive_data_unexpected_error)
                lineChart.visibility = GONE
                refreshBtn.visibility = GONE
                saveDataBtn.isEnabled = false
            }
            GraphDataStatus.DATA_IS_RECEIVING -> {
                graphDataStatus.text = getString(R.string.receiving_data)
                lineChart.visibility = GONE
                refreshBtn.visibility = GONE
                saveDataBtn.isEnabled = false

            }
            GraphDataStatus.DATA_SUCCESEFULLY_SAVED -> {
                graphDataStatus.text = getString(R.string.data_succesefully_saved)
            }
            GraphDataStatus.DATA_SUCCESEFULLY_OPENED -> {
                graphDataStatus.text = getString(R.string.data_succesefully_opened)
                saveDataBtn.isEnabled = true
                connectionButton.visibility = GONE
                refreshBtn.visibility = GONE
            }
        }
    }

    private fun drawData(data: List<Pair<Float, Float>>) {
        val chartEntries = mutableListOf<Entry>()
        for (coordinatesPair in data) {
            chartEntries.add(Entry(coordinatesPair.first, coordinatesPair.second))
        }
        val dataSet = LineDataSet(chartEntries, "Received graph")
        val lineData = LineData(dataSet)
        //docs: https://weeklycoding.com/mpandroidchart-documentation/
        lineChart.data = lineData
        lineChart.visibility = VISIBLE
        lineChart.description.isEnabled = false
        lineChart.invalidate() // refresh
    }

    private fun broadcastChosenFile(fileName: String) {
        setFragmentResult(
            FILE_IS_CHOSEN_AS_DATA_SOURCE_KEY, bundleOf()
        )
        setFragmentResult(
            CHOSEN_FILE_NAME_KEY, bundleOf(
                CHOSEN_FILE_NAME to fileName
            )
        )
    }

    companion object {
        const val FILE_IS_CHOSEN_AS_DATA_SOURCE_KEY = "FILE_IS_CHOSEN_AS_DATA_SOURCE_KEY"
        const val CHOSEN_FILE_NAME_KEY = "CHOSEN_FILE_NAME_KEY"
        const val CHOSEN_FILE_NAME = "CHOSEN_FILE_NAME"
    }

}