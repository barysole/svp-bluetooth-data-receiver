package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.adapter

import android.annotation.SuppressLint
import android.bluetooth.BluetoothClass
import android.graphics.drawable.AnimationDrawable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.R
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.enums.BTGroupInfoType
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model.BTDeviceModel
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.model.BTGroupInfo
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.adapter.BaseRecyclerAdapter
import cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.adapter.OnTouchIndicatorListener

class BtDeviceListAdapter() : BaseRecyclerAdapter<ViewHolder>() {

    private var items = arrayListOf<BtDeviceListItem>()
    var onDeviceItemClickListener: ((device: BTDeviceModel, position: Int) -> Unit)? = null
    var onUpdateGroupClickListener: ((btGroupInfoType: BTGroupInfoType) -> Unit)? = null

    fun setItems(items: ArrayList<BtDeviceListItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun addItems(items: ArrayList<BtDeviceListItem>) {
        val previousCount = itemCount
        for (item: BtDeviceListItem in items) {
            this.items.add(item)
        }
        notifyItemRangeInserted(previousCount, items.size)
    }

    fun pushLast(item: BtDeviceListItem) {
        this.items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun replaceItem(item: BtDeviceListItem, position: Int) {
        this.items[position] = item
        notifyItemChanged(position)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            0 -> {
                BtDeviceViewHolder(
                    layoutInflater.inflate(
                        R.layout.item_bt_device_list,
                        parent,
                        false
                    )
                )
            }
            1 -> {
                GroupTitleViewHolder(
                    layoutInflater.inflate(
                        R.layout.item_bt_group_list,
                        parent,
                        false
                    )
                )
            }
            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $viewType not supported")
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is BTDeviceModel -> {
                0
            }
            is BTGroupInfo -> {
                1
            }
            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $position not supported")
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (val item = items[position]) {
            is BTDeviceModel -> {
                (holder as BtDeviceViewHolder).bind(item)
            }
            is BTGroupInfo -> {
                (holder as GroupTitleViewHolder).bind(item)
            }
            else -> {
                throw IllegalArgumentException("onCreateViewHolder item $position not supported")
            }
        }
    }

    inner class BtDeviceViewHolder(view: View) : ViewHolder(view) {

        private val deviceName = view.findViewById<TextView>(R.id.list_name)
        private val additionalName = view.findViewById<TextView>(R.id.list_additional_name)
        private val description = view.findViewById<TextView>(R.id.list_description)
        private val deviceIcon = view.findViewById<ImageView>(R.id.list_icon)


        init {
            view.setOnClickListener {
                onDeviceItemClickListener?.invoke((items[adapterPosition] as BTDeviceModel), adapterPosition)
            }
        }

        fun bind(device: BTDeviceModel) {
            if (device.name.isNullOrEmpty()) {
                deviceName.text = device.macAddress
                additionalName.text = ""
            } else {
                deviceName.text = device.name
                additionalName.text = device.macAddress
            }
            if (device.isPairing) {
                description.visibility = VISIBLE
                description.text = context.getString(R.string.bt_pairing_info_string)
                description.setTextColor(context.getColor(R.color.purple_200))
                deviceName.setTextColor(context.getColor(R.color.purple_200))
                additionalName.setTextColor(context.getColor(R.color.purple_200))
                deviceIcon.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.ic_loader_animation
                    )
                )
                (deviceIcon.drawable as AnimationDrawable).start()
            } else {
                if (device.isChosen) {
                    description.visibility = VISIBLE
                    description.text = context.getString(R.string.bt_current_device_info_string)
                    description.setTextColor(context.getColor(R.color.purple_500))
                    deviceName.setTextColor(context.getColor(R.color.purple_500))
                    additionalName.setTextColor(context.getColor(R.color.purple_500))
                    deviceIcon.setColorFilter(context.getColor(R.color.purple_500))
                } else {
                    description.visibility = GONE
                    deviceName.setTextColor(context.getColor(R.color.text_default))
                    additionalName.setTextColor(context.getColor(R.color.text_default))
                    deviceIcon.setColorFilter(context.getColor(R.color.black))
                }
                //todo: move to utils
                when (device.majorDeviceType) {
                    BluetoothClass.Device.Major.COMPUTER -> {
                        deviceIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_baseline_computer_24))
                    }
                    BluetoothClass.Device.Major.AUDIO_VIDEO -> {
                        deviceIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_baseline_headphones_24))
                    }
                    BluetoothClass.Device.Major.PHONE -> {
                        deviceIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_baseline_smartphone_24))
                    }
                    else -> {
                        deviceIcon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_baseline_bluetooth_24))
                    }
                }
            }
        }

    }

    inner class GroupTitleViewHolder(view: View) : ViewHolder(view) {

        private var groupName = view.findViewById<TextView>(R.id.group_name)
        private var loader = view.findViewById<ImageView>(R.id.load_icon)

        @SuppressLint("ClickableViewAccessibility")
        fun bind(groupInfo: BTGroupInfo) {
            groupName.text = groupInfo.name
            if (groupInfo.isLoading) {
                loader.isClickable = false;
                loader.background = null;
                loader.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.ic_loader_animation
                    )
                )
                (loader.drawable as AnimationDrawable).start()
            } else {
                val standardLoaderBG =
                    AppCompatResources.getDrawable(context, R.drawable.ic_circle_outline_white)
                loader.background =
                    AppCompatResources.getDrawable(context, R.drawable.ic_circle_outline_white)
                loader.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.ic_baseline_autorenew_24
                    )
                )
                //todo: move to separate view
                loader.setOnTouchListener(
                    OnTouchIndicatorListener(
                        standardLoaderBG,
                        AppCompatResources.getDrawable(context, R.drawable.ic_circle_outline_gray)
                    )
                )
                loader.setOnClickListener {
                    onUpdateGroupClickListener?.invoke(groupInfo.byBTGroupInfoType)
                }
            }
        }

    }
}