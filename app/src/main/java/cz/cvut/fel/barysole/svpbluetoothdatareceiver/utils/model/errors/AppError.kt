package cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.model.errors

class AppError(override val message: String) : UserError {
}
