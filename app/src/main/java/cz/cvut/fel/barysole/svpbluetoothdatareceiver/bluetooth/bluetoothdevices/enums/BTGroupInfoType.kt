package cz.cvut.fel.barysole.svpbluetoothdatareceiver.bluetooth.bluetoothdevices.enums

enum class BTGroupInfoType {
    PAIRED_DEVICES,
    DISCOVERED_DEVICES
}