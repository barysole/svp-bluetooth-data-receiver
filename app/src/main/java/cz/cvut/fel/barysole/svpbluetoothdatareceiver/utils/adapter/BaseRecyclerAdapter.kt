package cz.cvut.fel.barysole.svpbluetoothdatareceiver.utils.adapter

import android.content.Context
import android.view.LayoutInflater

abstract class BaseRecyclerAdapter<VH : androidx.recyclerview.widget.RecyclerView.ViewHolder> : androidx.recyclerview.widget.RecyclerView.Adapter<VH>() {

    private var _context: Context? = null
    protected open val context
        get() = _context!!
    private var _layoutInflater: LayoutInflater? = null
    protected val layoutInflater
        get() = _layoutInflater!!

    override fun onAttachedToRecyclerView(recyclerView: androidx.recyclerview.widget.RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        _context = recyclerView.context
        _layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun onDetachedFromRecyclerView(recyclerView: androidx.recyclerview.widget.RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        _context = null
        _layoutInflater = null
    }
}